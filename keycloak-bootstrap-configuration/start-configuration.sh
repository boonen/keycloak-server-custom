#!/bin/bash
echo Starting Keycloak configuration

if [ -z $KEYCLOAK_SERVER_URI ]; then
    KEYCLOAK_SERVER_URI=http://keycloak-server/auth
fi
if [ -z $KEYCLOAK_REALM ]; then
    KEYCLOAK_REALM=master
fi

# Start Java based configuration.
java -Dkeycloak.server=${KEYCLOAK_SERVER_URI} -Dkeycloak.realm=${KEYCLOAK_REALM} \
  -Dkeycloak.user=${KEYCLOAK_USER} -Dkeycloak.password="${KEYCLOAK_PASSWORD}" \
  -Dkeycloak.environment.type=${KEYCLOAK_ENVIRONMENT_TYPE} \
  -jar java-configuration.jar

if [ $? -eq 0 ]
then
  echo "Keycloak configuration finished."
  exit 0
else
  echo "Keycloak configuration failed." >&2
  exit 1
fi
