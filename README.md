# Customized Keycloak Server

This project contains a PoC code base for a 0-CVE Keycloak setup with a configuration 
as code setup. It also demonstrates how you can add custom plugins and themes.

## Development

### Building steps

Run this in the root directory
```
mvn clean package
```

then switch to keycloak directory from root directory and execute
```
docker compose build
```

### Running the application

Once the Maven build has completed successfully, you can start the necessary Docker
containers from the docker compose file.

```
docker compose up
```

Keycloak is available at http://localhost:9090.

### Debugging the application

Remote debugging is available on port `5005`. In IntelliJ you can configure the debugger as
follows:

![img.png](docs/intellij_keycloak_debugging.png)